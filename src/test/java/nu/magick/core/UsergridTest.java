package nu.magick.core;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpMethod;
import org.usergrid.java.client.Client;
import org.usergrid.java.client.entities.Entity;
import org.usergrid.java.client.entities.User;
import org.usergrid.java.client.response.ApiResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * @author Cristian Miranda
 * @since 7/22/14 - 16:15
 */
public class UsergridTest {

	/**
	 * Useful links:
	 *
	 *  - Code and sources  : https://github.com/usergrid/usergrid/tree/master
	 * 	- Cloud instance    : https://apigee.com
	 * 	- Management API    : https://apigee.com/docs/management/apis
	 * 						: http://apigee.com/docs/app-services/content/rest-endpoints
	 *  - Data queries      : https://apigee.com/docs/app-services/content/querying-your-app-services-data
	 *  - Usergrid Java API : http://mvnrepository.com/artifact/org.usergrid/usergrid-java-client/0.0.3
	 *	- Getting started   : http://usergrid.incubator.apache.org/docs/getting-up-and-running-locally/
	 *	- Organizations     : http://apigee.com/docs/app-services/content/organization
	 *
	 *
	 * - For running this test:
	 *
	 * Start by creating an Organization.
	 * It’s the top-level structure in Usergrid: all Apps and Administrators must belong to an Organization.
	 * Here’s how you create one:
	 *
	 *   curl -X POST  \
	 *   -d 'organization=myfirstorg&username=myadmin&name=Admin&email=admin@example.com&password=password' \
	 *   http://localhost:8080/management/organizations
	 *
	 * You can see that creating an Organization creates an Administrator in the process. Let’s authenticate as him:
	 *
	 *   curl 'http://localhost:8080/management/token?grant_type=password&username=myadmin&password=password'
	 *   This will return an access_token. We’ll use this to authenticate the next two calls. Next, let’s create an Application:
	 *
	 *   curl -H "Authorization: Bearer [the management token from above]" \
	 *   -H "Content-Type: application/json" \
	 *   -X POST -d '{ "name":"myapp" }' \
	 *   http://localhost:8080/management/orgs/myfirstorg/apps
	 *
	 * @author Cristian Miranda
	 * @since 2014-07-22 17:44
	 */
	@Test
	public void test_creatingUserAndCollections(){

		String organizationId = "myfirstorg";
		String applicationId = "myapp";
		String username = "cmiranda";
		String email = "crism60@gmail.com";
		String password = "someMagickPassword";

		Client client = new Client(organizationId, applicationId);
		client.setApiUrl(Client.LOCAL_STANDALONE_API_URL);

		client.createUser(username, "Cristian", email, password);
		ApiResponse response = client.authorizeAppUser(email, password);

		System.out.println(response.getAccessToken());
		User user = response.getUser();

		Entity entity = new Entity();
		entity.setUuid(UUID.randomUUID());
		entity.setProperty("someKey", "someValue");
		entity.setType("collection");
		client.createEntity(entity);

		response = client.queryEntitiesRequest(HttpMethod.GET, null, null, organizationId, applicationId, "collection").getResponse();
		Assert.assertTrue(response.getEntityCount() > 0);

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ql", "someKey='someValue'");
		client.queryEntitiesRequest(HttpMethod.DELETE, params, null, organizationId, applicationId, "collection").getResponse();

		response = client.queryEntitiesRequest(HttpMethod.GET, null, null, organizationId, applicationId, "collection").getResponse();
		Assert.assertTrue(response.getEntities().isEmpty());

	}

}
